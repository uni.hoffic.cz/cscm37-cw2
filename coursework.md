# CSCM37 - CW1

- **Petr Hoffmann**
- **963810**

# Insight 0

## Schema

### Aim

This visualisation aims to show that the person depicted in the visualisation had a glass eye.

### Visualisation Type

Direct Volume Rendering

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Opacity ~ Material density

### Data Preparation

N/A

### Limitations

- The eye prothesis is made of a similarly dense material as other parts of the head and therefore it's not possible to
  distinguish it using only the colour.
- The visualisation does not show the normal eye for comparison.

### Final Form

![](images/d0_5.png)

## Evolution

We start by showing the volume representation of the data.

![](images/d0_1.png)

We calculate a histogram to find out how to set up filters for surface calculation.

![](images/d0_2.png)

We use the values obtained from the histogram to create a surface representation of the brain.

![](images/d0_3.png)

The data resolution is not suitable for this operation, and so we switch to contouring instead.

![](images/d0_4.png)

Contouring this data due to its resolution and dynamic range too doe not yield satisfactory visualisation. We therefore
revert to direct volume rendering and adjust the color mapping to hide the outer layer of the head. This shows the
prothesis eye and the hides the normal eye.

![](images/d0_5.png)

# Insight 1

## Schema

### Aim

This visualisation aims to show that the object captured in the data provided is a teddy bear. This is the case, because
the object has a label on its chest, so it is not a real bear.

### Visualisation Type

Direct Volume Rendering

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Opacity ~ Fitting in the range of the material density

### Data Preparation

N/A

### Limitations

- We cannot see inside the teddy bear to distinguish the materials used.
- The base that the teddy bear is laying on is also visible, obstructing the view somewhat.
- The use of one colour makes it a little difficult to see any detail apart from the basic shape and very distinct
  materials.

### Final Form

![](images/d1_3.png)

## Evolution

We start with importing the data and seeing how it looks like in volume view:

![](images/d1_1.png)

By rotating it and inspecting it from sides, we gain a suspicion that one of the axes is not scaled appropriately. We
therefore adjust the scale.

![](images/d1_2.png)

Now we recognize the outline of the object, but there is a lot of noise, so we adjust the opacity mapping to make air
transparent.

![](images/d1_3.png)

We can now easily recognise the object is a teddy bear. We clearly see a label on the object's chest, a rectangle cloth
used for the bear's palm, and the lines that join individual surfaces that make up the bear's skin/surface.

# Insight 2

## Schema

### Aim

This visualisation aims to show that the teddy bear's label is made out of a different material than the surface, and so
are its palms, and that the inside of the object is filled with homogenous material with low measured material density.

### Visualisation Type

Surface Fitting

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Colour ~ Material density

### Data Preparation

We calculated a histogram for the material density values in order to filter out air and the base the bear is laying on.

### Limitations

- The surface was calculated using the nearest neighbour method, and the surface is therefore not smooth.
- The final form is a combination of the surface fitting of the bear and volume rendering of the rest of the data. This
  allows to show the eyes of the bear made of a high density material, but it also shows the base the bear is laying on
  since it has a similar material density.
- The visualisation does not directly show the inside of the bear, but rather the empty space after filtering out low
  density materials.

### Final Form

![](images/d2_4.png)

## Evolution

We start by importing the data and using a threshold filter to filter out the air and the materials that make out the
base the object is laying at.

![](images/d2_1.png)

Then we adjust the color map to the newly filtered range.

![](images/d2_2.png)

We notice that the arm of the bear is hollow, even though only the lower density material values have been filtered out.
This indicates that the bear is full of soft or fluffy material.

We bring the direct volume rendering in the scene to show all materials except the lowest densities to reinforce that
the label, palm pads and the insides of the bear is, indeed, a low density, fluffy material rather than a dense organ as
it could be the case with the eyes.

![](images/d2_3.png)

Finally, we show a histogram indicating what range of material densities has been used to render the bear (blue
rectangle). The highest spike represents the air around the object and partly the filament of the bear. The 2nd highest
spike represents the base the bear is laying on. The hill on the right is for a further mat that be bear is placed on.

![](images/d2_4.png)

# Insight 3

## Schema

### Aim

This visualisation aims to show that the data represents a fish.

### Visualisation Type

Direct Volume Rendering

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Colour ~ Material density

### Data Preparation

N/A

### Limitations

- The visualisation only shows the surface and provides no information about the inside structure of the object.
- Since this visualisation only shows the shape of the fish, the colours could have been chosen to better match how a
  fish would be coloured.

### Final Form

![](images/d3_2.png)
![](images/d3_3.png)

## Evolution

We start with importing the data and visualising the volume.

![](images/d3_1.png)

Immediately, the visualisation shows clearly that we are looking at a fish.

Although it looks really cool having the air shown as water in blue, we remove it for a better look at the shape of the
object.

![](images/d3_2.png)
![](images/d3_3.png)

We see a highly detailed representation of a fish from multiple angles. It looks real, but we cannot tell without
looking inside.

# Insight 4

## Schema

### Aim

This visualisation aims to show that the fish represented by the data was unlikely to be a toy, but rather a real
animal. This is the case because of the clear structure of bones visible, and an indication of certain large organs.

### Visualisation Type

Surface Fitting

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Colour ~ Material density

### Data Preparation

N/A

### Limitations

- The visualisation does not show all organs, for example muscles or veins are missing.
- The visualisation shows an oval shape inside the eye of the fish. As a non-biologist I believe animals don't have a
  bone inside their eyes, and therefore I assume that the material depicted as bone also includes other, similar
  materials.

### Final Form

![](images/d4_6.png)
![](images/d4_7.png)

## Evolution

We start with loading the data and fitting a surface through the most common value in the data set that is not air or a
piece of measuring equipment.

![](images/d4_1.png)

We see a bone structure of the fish. We try displaying it in the context of the rest of the fish by fitting a linear
range of 10 surfaces through the data.

![](images/d4_2.png)

We notice the surface of the fish is visible, but only the very outer surface, and no indications of organs either.

We try instead fitting a series of surfaces logarithmically to the density.

![](images/d4_3.png)

Now we see the organs. There are still hazy surfaces that do not display a clear structure. We remove them.

![](images/d4_5.png)

Now we can see the bone structure and the larger organs well. We try adjusting the color mapping to better highlight the
organs.

![](images/d4_6.png)
![](images/d4_7.png)

Now the visualisation allows us to make an argument that the fish is a real animal.
