# Insight 2

## Schema

### Aim

This visualisation aims to show that the teddy bear's label is made out of a different material than the surface, and so
are its palms, and that the inside of the object is filled with homogenous material with low measured material density.

### Visualisation Type

Surface Fitting

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Colour ~ Material density

### Data Preparation

We calculated a histogram for the material density values in order to filter out air and the base the bear is laying on.

### Limitations

- The surface was calculated using the nearest neighbour method, and the surface is therefore not smooth.
- The final form is a combination of the surface fitting of the bear and volume rendering of the rest of the data. This
  allows to show the eyes of the bear made of a high density material, but it also shows the base the bear is laying on
  since it has a similar material density.
- The visualisation does not directly show the inside of the bear, but rather the empty space after filtering out low
  density materials.

### Final Form

![](images/d2_4.png)

## Evolution

We start by importing the data and using a threshold filter to filter out the air and the materials that make out the
base the object is laying at.

![](images/d2_1.png)

Then we adjust the color map to the newly filtered range.

![](images/d2_2.png)

We notice that the arm of the bear is hollow, even though only the lower density material values have been filtered out.
This indicates that the bear is full of soft or fluffy material.

We bring the direct volume rendering in the scene to show all materials except the lowest densities to reinforce that
the label, palm pads and the insides of the bear is, indeed, a low density, fluffy material rather than a dense organ as
it could be the case with the eyes.

![](images/d2_3.png)

Finally, we show a histogram indicating what range of material densities has been used to render the bear (blue
rectangle). The highest spike represents the air around the object and partly the filament of the bear. The 2nd highest
spike represents the base the bear is laying on. The hill on the right is for a further mat that be bear is placed on.

![](images/d2_4.png)
