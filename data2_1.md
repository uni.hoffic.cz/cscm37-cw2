# Insight 3

## Schema

### Aim

This visualisation aims to show that the data represents a fish.

### Visualisation Type

Direct Volume Rendering

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Colour ~ Material density

### Data Preparation

N/A

### Limitations

- The visualisation only shows the surface and provides no information about the inside structure of the object.
- Since this visualisation only shows the shape of the fish, the colours could have been chosen to better match how a
  fish would be coloured.

### Final Form

![](images/d3_2.png)
![](images/d3_3.png)

## Evolution

We start with importing the data and visualising the volume.

![](images/d3_1.png)

Immediately, the visualisation shows clearly that we are looking at a fish.

Although it looks really cool having the air shown as water in blue, we remove it for a better look at the shape of the
object.

![](images/d3_2.png)
![](images/d3_3.png)

We see a highly detailed representation of a fish from multiple angles. It looks real, but we cannot tell without
looking inside.
