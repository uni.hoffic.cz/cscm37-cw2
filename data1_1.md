# Insight 1

## Schema

### Aim

This visualisation aims to show that the object captured in the data provided is a teddy bear. This is the case, because
the object has a label on its chest, so it is not a real bear.

### Visualisation Type

Direct Volume Rendering

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Opacity ~ Fitting in the range of the material density

### Data Preparation

N/A

### Limitations

- We cannot see inside the teddy bear to distinguish the materials used.
- The base that the teddy bear is laying on is also visible, obstructing the view somewhat.
- The use of one colour makes it a little difficult to see any detail apart from the basic shape and very distinct
  materials.

### Final Form

![](images/d1_3.png)

## Evolution

We start with importing the data and seeing how it looks like in volume view:

![](images/d1_1.png)

By rotating it and inspecting it from sides, we gain a suspicion that one of the axes is not scaled appropriately. We
therefore adjust the scale.

![](images/d1_2.png)

Now we recognize the outline of the object, but there is a lot of noise, so we adjust the opacity mapping to make air
transparent.

![](images/d1_3.png)

We can now easily recognise the object is a teddy bear. We clearly see a label on the object's chest, a rectangle cloth
used for the bear's palm, and the lines that join individual surfaces that make up the bear's skin/surface.
