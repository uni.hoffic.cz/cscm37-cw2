# Insight 0

## Schema

### Aim

This visualisation aims to show that the person depicted in the visualisation had a glass eye.

### Visualisation Type

Direct Volume Rendering

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Opacity ~ Material density

### Data Preparation

N/A

### Limitations

- The eye prothesis is made of a similarly dense material as other parts of the head and therefore it's not possible to
  distinguish it using only the colour.
- The visualisation does not show the normal eye for comparison.

### Final Form

![](images/d0_5.png)

## Evolution

We start by showing the volume representation of the data.

![](images/d0_1.png)

We calculate a histogram to find out how to set up filters for surface calculation.

![](images/d0_2.png)

We use the values obtained from the histogram to create a surface representation of the brain.

![](images/d0_3.png)

The data resolution is not suitable for this operation, and so we switch to contouring instead.

![](images/d0_4.png)

Contouring this data due to its resolution and dynamic range too doe not yield satisfactory visualisation. We therefore
revert to direct volume rendering and adjust the color mapping to hide the outer layer of the head. This shows the
prothesis eye and the hides the normal eye.

![](images/d0_5.png)
