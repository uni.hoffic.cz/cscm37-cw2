# Insight 4

## Schema

### Aim

This visualisation aims to show that the fish represented by the data was unlikely to be a toy, but rather a real
animal. This is the case because of the clear structure of bones visible, and an indication of certain large organs.

### Visualisation Type

Surface Fitting

### Visualisation Mapping

- XYZ coordinates ~ XYZ coordinates of the scanned object
- Colour ~ Material density

### Data Preparation

N/A

### Limitations

- The visualisation does not show all organs, for example muscles or veins are missing.
- The visualisation shows an oval shape inside the eye of the fish. As a non-biologist I believe animals don't have a
  bone inside their eyes, and therefore I assume that the material depicted as bone also includes other, similar
  materials.

### Final Form

![](images/d4_6.png)
![](images/d4_7.png)

## Evolution

We start with loading the data and fitting a surface through the most common value in the data set that is not air or a
piece of measuring equipment.

![](images/d4_1.png)

We see a bone structure of the fish. We try displaying it in the context of the rest of the fish by fitting a linear
range of 10 surfaces through the data.

![](images/d4_2.png)

We notice the surface of the fish is visible, but only the very outer surface, and no indications of organs either.

We try instead fitting a series of surfaces logarithmically to the density.

![](images/d4_3.png)

Now we see the organs. There are still hazy surfaces that do not display a clear structure. We remove them.

![](images/d4_5.png)

Now we can see the bone structure and the larger organs well. We try adjusting the color mapping to better highlight the
organs.

![](images/d4_6.png)
![](images/d4_7.png)

Now the visualisation allows us to make an argument that the fish is a real animal.
